---
layout: handbook-page-toc
title: "TAM Webinar Calendar"
---
## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

View the [TAM Handbook homepage](/handbook/customer-success/tam/) for additional TAM-related handbook pages.

---

## Upcoming Webinars

We’d like to invite you to our free upcoming webinars during the month of May.

If you know someone who might be interested in attending, feel free to share the registration links with them. Everyone is welcome, and we hope to see you there!


### May 2022

### Advanced CI/CD 
#### May 24th, 2022 at 11:00AM-12:00PM Eastern Time/3:00-4:00 PM UTC

Expand your CI/CD knowledge while we cover advanced topics that will accelerate your efficiency using GitLab, such as pipelines, variables, rules, artifacts, and more. This session is intended for those who have used CI/CD in the past.
 
[<button class="btn btn-primary" type="button">Register</button>](https://gitlab.zoom.us/webinar/register/WN_aWj_kz5XSY6rJg5gU82Sww)

### Introducción a GitLab
#### May 24th, 2022 at 3:00PM-4:00PM Eastern Time/7:00-8:00 PM UTC

¿Estas comenzando con GitLab? Te invitamos a este webinar, donde revisaremos qué es GitLab, cómo lo beneficia y el flujo de trabajo recomendado para permitirle aprovechar al máximo la plataforma.

[<button class="btn btn-primary" type="button">Register</button>](https://gitlab.zoom.us/webinar/register/WN_cp7RI389RpScOixaSe1wyA)

### Introducción a CI/CD con GitLab
#### May 26th, 2022 at 3:00PM-4:00PM Eastern Time/7:00-8:00 PM UTC

Ven y conoce qué es CI/CD y cómo puede beneficiar a tu equipo/organización. Cubriremos una descripción general de CI/CD y cómo se ve en GitLab. También cubriremos cómo comenzar con su primer pipeline de CI/CD en GitLab y los conceptos básicos de los GitLab Runners.

[<button class="btn btn-primary" type="button">Register</button>](https://gitlab.zoom.us/webinar/register/WN_ztNbL4O3RSmBQjZSyYTQJg)

### DevSecOps/Compliance 
#### May 31st, 2022 at 11:00AM-12:00PM Eastern Time/3:00-4:00 PM UTC

GitLab enables developers and security to work together in a single tool, allowing for proactive security or “shifting left”. This session will cover what GitLab offers, how scan results integrate seamlessly with merge requests, and how to use the Security Dashboard to manage vulnerabilities.
 
[<button class="btn btn-primary" type="button">Register</button>](https://gitlab.zoom.us/webinar/register/WN_tZ7ZCQSxRVaH8fwU8I9PSg)
